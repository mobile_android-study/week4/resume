import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

Row _buildHeader(String label) {
  return Row(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Container(
        color: Colors.yellow[700],
        child: Text(
          label,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
        ),
        margin: EdgeInsets.fromLTRB(32, 16, 0, 0),
      )
    ],
  );
}
Column _buildIconColumn(String iconPath, String label) {
  return Column(mainAxisSize: MainAxisSize.min,
  children: [
    Image.asset(
      iconPath,
      width: 42,
      height: 42,
      fit: BoxFit.fitHeight,
    ),
    Container(
      margin: EdgeInsets.only(top: 4),
      child: Text(label,
          style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.bold,
              color: Colors.grey[700])),
    )
  ]);
}
Row _buildIconRow(String iconPath, String label) {
  return Row(mainAxisAlignment: MainAxisAlignment.start, children: [
    Container(
      margin: EdgeInsets.fromLTRB(48, 8, 8, 8),
    child: Image.asset(
      iconPath,
      width: 42,
      height: 42,
      fit: BoxFit.fitHeight,
    )),
    Container(
      child: Text(label,
          style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.bold,
              color: Colors.grey[700])),
    )
  ]);
} 

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget basicBio = Container(
      padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Sukit Wiriyakesamongkol',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text('Nun', style: TextStyle(color: Colors.grey[500])),
              Container(
                child: Row(
                  children: [
                    Icon(
                      Icons.facebook,
                      color: Colors.blue[600],
                    ),
                    Text(' /nun.kyo')
                  ],
                ),
              ),
            ],
          ),
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Container(
              child: Row(
                children: [
                  Icon(
                    Icons.male,
                    color: Colors.blue[500],
                  ),
                  Text(' Male'),
                ],
              ),
            ),
            Container(
              child: Row(
                children: [
                  Icon(
                    Icons.cake,
                    color: Colors.pink[200],
                  ),
                  Text(' 4th March 1998')
                ],
              ),
            ),
          ]),
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Container(
              child: Row(
                children: [
                  Icon(
                    Icons.call,
                    color: Colors.green[200],
                  ),
                  Text(' 080-5517333'),
                ],
              ),
            ),
            Container(
              child: Row(
                children: [
                  Icon(
                    Icons.email,
                    color: Colors.blue[300],
                  ),
                  Text(' livxenon@gmail.com')
                ],
              ),
            ),
          ]),
        ],
      ),
    );
    Widget textSection = Container(
        margin: EdgeInsets.fromLTRB(48, 8, 36, 8),
        child: Text(
          'Experieced in English comunication at intermidiate to high level.'
          'Have passion in programming and data like web, mobile and database.'
          'Studying at Burapha university, faculty of technology, Computer Science Major.'
          'Dedicated, puntual, ambitious, well-organized and outgoing. Co-working '
          'and contacting are capable. Accept mistakes and always want to develop self',
          softWrap: true,
        ));
    Widget skillBio = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildIconColumn('../images/java.png', 'Java'),
          _buildIconColumn('../images/py.png', 'Python'),
          _buildIconColumn('../images/Vue.png', 'Vuejs'),
          _buildIconColumn('../images/html5.png', 'html5'),
          _buildIconColumn('../images/sql.png', 'SQL'),
        ],
      ),
    );
    Widget eduBio = Container(
      child: Column(
        children: [
          _buildIconRow('../images/Buu-logo.png',
          ' Burapha University, Informatics major of Computer Science since 2019'),
          _buildIconRow('../images/SL.jpg',
          ' Saint Louis School Chachoengsao, Secondary class since 2012'),
          _buildIconRow('../images/SL.jpg',
          ' Saint Louis School Chachoengsao, Primary class since 2008'),
        ],
      ),
    );

    return MaterialApp(
        title: 'Resume',
        home: Scaffold(
          appBar: AppBar(
            title: const Text('Resume'),
            backgroundColor: Colors.yellow[700],
          ),
          body: ListView(children: [
            Image.asset('../images/profile.jpg',
                width: 240, height: 240, fit: BoxFit.fitHeight),
            _buildHeader('About'),
            basicBio,
            _buildHeader('Skills'),
            skillBio,
            _buildHeader('Bio'),
            textSection,
            _buildHeader('Education'),
            eduBio,
          ]),
        ));
  }
}
